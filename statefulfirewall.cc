/*
 * statefulipfilter.{cc,hh} -- Stateful IP-packet filter
 *
 */
#include <netinet/in.h>
#include <sstream>
#include <cstdlib>
#include <iostream>
#include <string>
#include <fstream>
#include <click/config.h>
#include <click/confparse.hh>
#include <click/args.hh>
#include "statefulfirewall.hh"

/* Add header files as required*/
CLICK_DECLS

/* 
Connection Methods
*/

bool Connection::is_forward()
{
	return this->isfw;
}

/* 
Policy Methods
*/
/* Return a Connection object representing policy */
Connection Policy::getConnection()
{
	return Connection(this->sourceip, this->destip, this->sourceport, this->destport, this->proto, this->action);
}
 
/* Return action for this Policy */
int Policy::getAction()
{
	return this->action;
}

/* 
StatefulFirewall methods
*/
int StatefulFirewall::configure(Vector<String> &conf, ErrorHandler *errh)
{
	// log input
	// click_chatter("configure %s and %s", conf.at(0).c_str(), conf.at(1).c_str());
	String pfile = "";

	if (Args(this, errh).bind(conf)
        .read("POLICYFILE", pfile)
        .read("DEFAULT", DEFAULTACTION)
        .consume() < 0)
        return -1;

	// click_chatter("parse to %s and %d", pfile.c_str(), DEFAULTACTION);

        this->read_policy_config(pfile);
	return 0;
}

/* return true if Packet represents a new connection
     * i.e., check if the connection exists in the map.
     * You can also check the SYN flag in the header to be sure.
     * else return false.
     * Hint: Check the connection ID database.
     */
bool StatefulFirewall::check_if_new_connection( const Packet *p )
{
	Connection c = this->get_canonicalized_connection(p);
	if ( Connections.find(c) == Connections.end() ) {
  		// not found
		return true;	
	} else {
  		// found
		return false;
	}

}

    /*Check if the packet represent Connection reset
     * i.e., if the RST flag is set in the header.
     * Return true if connection reset
     * else return false.*/
bool StatefulFirewall::check_if_connection_reset(const Packet *p)
{
	const click_ip *iph = p->ip_header();
	uint8_t pr = iph->ip_p;
	// #define IP_PROTO_TCP            6
	if (pr == 6){   
		const click_tcp *tcph = p->tcp_header();
		uint8_t flag = tcph->th_flags;
		// #define TH_RST          0x04
		if (flag == 0x04)
		{
			// click_chatter("RST detected");
			return true;
		} 
	}
	return false;
}

    /* Add a new connection to the map along with its action.*/
void StatefulFirewall::add_connection(Connection &c, int action)
{
	Connections.insert( std::make_pair( c, action ) );
}

    /* Delete the connection from map*/
void StatefulFirewall::delete_connection(Connection &c)
{
	Connections.erase(c);
}

/* Create a new connection object for Packet.
 * Make sure you canonicalize the source and destination ip address and port number.
 * i.e, make the source less than the destination and
 * update isfw to false if you have to swap source and destination.
 * return NULL on error. */
Connection StatefulFirewall::get_canonicalized_connection(const Packet *p)
{
	const click_ip *iph = p->ip_header();
	IPAddress sip = IPAddress(iph->ip_src); 
	IPAddress dip = IPAddress(iph->ip_dst);	
	String s = sip.unparse();
	String d = dip.unparse();
	uint8_t pr = iph->ip_p;
	// #define IP_PROTO_ICMP           1
	// if (pr == 1){
	// 	const click_icmp *icmph = p->icmp_header();
	// 	uint16_t sp = icmph 
	// }
	uint16_t sp,dp;
	// #define IP_PROTO_TCP            6
	if (pr == 6){
		const click_tcp *tcph = p->tcp_header();
		sp = tcph->th_sport; 
		dp = tcph->th_dport; 
	}
	// #define IP_PROTO_UDP            17
	if (pr == 17){
		const click_udp *udph = p->udp_header();
		sp = udph->uh_sport; 
		dp = udph->uh_dport; 
	}
	int sp_i = ntohs(sp);
	int dp_i = ntohs(dp); 
	int pr_i = pr;
	int act = this->DEFAULTACTION;
	// canonicaliue
		
	// click_chatter("canonicalized %s %d %s %d %d %d", s.c_str(), sp_i, d.c_str(), dp_i, pr_i, act);
	return Connection(s, d, sp_i, dp_i, pr_i, act);	
}

/* Read policy from a config file whose path is passed as parameter.
     * Update the policy database.
     * Policy config file structure would be space separated list of
     * <source_ip source_port destination_ip destination_port protocol action>
     * Add Policy objects to the list_of_policies
     * */
int StatefulFirewall::read_policy_config(String s)
{
	string src, dst;
	int sp, dp, pro, act;
	string line;
	// click_chatter("myfile %s", ("\""+s+"\"").c_str());
  	ifstream myfile(s.c_str());
  	if (myfile.is_open())
  	{
		// skip first line
		getline (myfile,line);
    		while ( getline (myfile,line) )
    		{
			// click_chatter("line %s", line.c_str());
			// split string
			vector<string> line_vs;
			istringstream iss(line);
			while (iss) {
				string tmp;
				iss >> tmp;
				click_chatter("tmp %s",tmp.c_str());
				line_vs.push_back(tmp);
			}
			src = line_vs.at(0);
			dst = line_vs.at(2);
			sp = atoi((line_vs.at(1)).c_str());
			dp = atoi((line_vs.at(3)).c_str());
			pro = atoi((line_vs.at(4)).c_str());
			act = atoi((line_vs.at(5)).c_str());
			// click_chatter("atoi %s %s %d %d %d %d", src.c_str(), dst.c_str(),sp,dp,pro,act);
        		this->list_of_policies.push_back( Policy(String(src.c_str()), String(dst.c_str()), sp, dp, pro, act) );
    		}
    		myfile.close();
  	}
	else
		click_chatter("file can not be opened");
	
	return 0; // TODO check file open state
}

    /* Convert the integer ip address to string in dotted format.
     * Store the string in s.
     *
     * Hint: ntohl could be useful.*/
void StatefulFirewall::dotted_addr(const uint32_t *addr, char *s){
	// use IPAddress class instead of this.
}

   /* Check if Packet belongs to new connection.
    * If new connection, apply the policy on this packet
    * and add the result to the connection map.
    * Else return the action in map.
    * If Packet indicates connection reset,
    * delete the connection from connection map.
    *
    * Return 1 if packet is allowed to pass
    * Return 0 if packet is to be discarded
    */
int StatefulFirewall::filter_packet(const Packet *p){
	bool isnew = this->check_if_new_connection(p);
  	Connection c = this->get_canonicalized_connection(p);
	int act = this->DEFAULTACTION;
	if (isnew){
		// apply policy
		for (std::vector<Policy>::iterator it = this->list_of_policies.begin(); it != list_of_policies.end(); ++it){
			if ( ((*it).getConnection()).compare(c) == 0 )
				act = (*it).getAction();
		}

		// add to connection map
		this->add_connection(c, act);
	}
	else{
		// find action in map
		std::map<Connection,int, cmp_connection>::iterator it_map; 
		it_map = this->Connections.find(c);
		act = it_map->second;

		// check connection reset
		bool isReset = this->check_if_connection_reset(p);
		if (isReset)
		{
			this->delete_connection(c);
		}
	}
 
        // click_chatter("filter_packet action %d",act);
	return act;
}

  /* Push valid traffic on port 1
    * Push discarded traffic on port 0*/
void StatefulFirewall::push(int port, Packet *p){
	int isallow = this->filter_packet(p);

	// check p valid
	// click_chatter("push to port %d",isallow);
	if (isallow == 1) output(1).push(p);
	else output(0).push(p);
}


CLICK_ENDDECLS
EXPORT_ELEMENT(StatefulFirewall)
